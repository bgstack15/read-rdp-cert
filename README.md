# Readme for `read_rdp_cert`

## Overview
Remote Desktop Protocol (rdp) has the ability to use TLS certificates to encrypt the traffic. Unfortunately, the traditional trick with `openssl s_client -connect rpdhost.example.com:3389` does not work due to how RDP has some communication before engaging the tls components.

This project can read a packet capture, really any pcap that contains the TLSv1 Certificate protocol, and save from the TLSv1 Certificates packets any pem-format certificates to disk. Of course this project is open-source, so you can adapt it to do whatever you want.


## Using
You need to generate a packet capture file, which can be done with `wireshark` or `tcpdump`. I used filters:

    sudo tcpdump -w ~/packets.in -n -v -A "port 3389 and (tcp[((tcp[12] & 0xf0) >> 2)] = 0x16)"

This filter is taken from [Reference 2](https://stackoverflow.com/questions/39624745/capture-only-ssl-handshake-with-tcpdump).

In wireshark, you can even use a display filter of `tls.handshake.type == 11` as well.

With the output file `~/packets.in`, run the main utility with this file as a parameter.

    ./read_rdp_cert.py --pcapfile ~/packets.in

The utility will extract all certificates that it can find from the tls handshake packets, into the current directory.

## Reason for existence
I have not found on the Internet appears how to read RDP certificates.

## Alternatives
I have not researched these alternatives thoroughly, but they showed possible alternative libraries to use:
* [https://github.com/thy09/isolation/blob/master/load_cert.py](https://github.com/thy09/isolation/blob/master/load_cert.py)
* pyshark lib [https://security.stackexchange.com/questions/123851/how-can-i-extract-the-certificate-from-this-pcap-file](https://security.stackexchange.com/questions/123851/how-can-i-extract-the-certificate-from-this-pcap-file)

## References
1. The `iplayer_from_raw` function is almost directly from [cuckoolinux -> network.py](https://github.com/0x71/cuckoo-linux/blob/82263c5df40ebe70dc35976b917293eb54a363af/modules/processing/network.py) and is licensed GPL-3.
2. [https://stackoverflow.com/questions/39624745/capture-only-ssl-handshake-with-tcpdump](https://stackoverflow.com/questions/39624745/capture-only-ssl-handshake-with-tcpdump)

## License
GPL-3

## Dependencies
Distro    | Packages
--------- | --------------------------------
Fedora 33 | python3-pyOpenSSL, python3-dpkt
Devuan    | python3-openssl, python3-dpkt

Tested against python 3.9.2 but probably could be lowered if you replace the f"" strings (started with python 3.6).
