#!/usr/bin/env python3
# File: read_rdp_cert.py
# Location: https://gitlab.com/bgstack15/read-rdp-cert
# Author: bgstack15
# Startdate: 2021-07-28 14:02
# Title: Read RDP Certificate Used from a Packet Capture File
# Purpose: Given pcap input file that contains a TLS HANDSHAKE CERTIFICATE packet, extract out the cert
# History:
# Usage:
#    Generate packet capture with:
#       sudo tcpdump -w ~/packets.in -n -v -A "port 3389 and (tcp[((tcp[12] & 0xf0) >> 2)] = 0x16)"
#    Then visit rdp.
#       yes no | xfreerdp destserver.internal.example.com
#    Then submit the packet capture file to read_rdp_cert.py
#       ./read_rdp_cert.py --pcapfile ~/packets.in
# Reference:
# Improve:
#    Add debug level

# Note: if I need libpath logic, check logout-manager-cli
from rrc_lib import *
import argparse

read_rdp_cert_version="2021-07-29"

parser = argparse.ArgumentParser(description="read pcap files and extract TLSv1 Certificate certificates")
parser.add_argument("-p","--pcapfile", required=True, help="Input file. Required.")
parser.add_argument("-V","--version", action="version", version="%(prog)s " + read_rdp_cert_version)

args = parser.parse_args()

array = read_pcap_file(args.pcapfile)
for i in array:
   save_cert(
      data = i,
      directory = os.path.curdir
   )
